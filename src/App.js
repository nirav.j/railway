import Routes from "./Routes";
import { ToastContainer } from "react-toastify";

const App = () => {
  return (
    <>
      <Routes />
      <ToastContainer />
    </>
  );
};

export default App;
