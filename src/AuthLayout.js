import { Redirect, Route, Switch } from 'react-router-dom';

import Dashboard from './components/Dashboard';
import Header from './components/Header';
import Navbar from './components/Navbar';
import PlanJourney from './components/PlanJourney';
import ResponsiveMenu from './components/ResponsiveMenu';

const AuthLayout = () => {
  return (
    <div className="main-dashboard">
      <div className="navbar-left">
        <Navbar />
      </div>
      <ResponsiveMenu />
      <div className="main-body-part-right">
        <Header />
        <Switch>
          <Route path="/dashboard" component={Dashboard} />
          <Route path="/plan-journey" component={PlanJourney} />
          <Redirect to="/" />
        </Switch>
      </div>
    </div>
  );
};

export default AuthLayout;
