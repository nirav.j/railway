import * as Yup from 'yup';

export const validator = type => {
  switch (type) {
    case 'emailRequired':
      return Yup.string()
        .email('Expected input: email')
        .required('This field is Required');
    case 'phone':
      const phoneRegExp =
        /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

      return Yup.string()
        .matches(phoneRegExp, 'Phone number is not valid')
        .required('This field is Required');
    case 'password':
      const passwordExp =
        /^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/;
      return Yup.string()
        .required('This field is Required')
        .matches(
          passwordExp,
          'Password Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and one special case Character',
        );
    case 'passwordConfirmation':
      return Yup.string()
        .required('This field is Required')
        .oneOf([Yup.ref('password'), null], 'Password should be same.');

    case 'required':
    default:
      return Yup.string().required('This field is Required');
  }
};

export const displayFormErrors = (
  key = '',
  errors = {},
  touched = {},
  submitCount = 1,
) => {
  if (errors[key] !== undefined && errors[key] && submitCount) {
    return (
      <span className="text-danger input-feedback font12 input-error">
        {errors[key]}
      </span>
    );
  }
  return null;
};

export const loginFormValidation = () => {
  return Yup.object().shape({
    email: validator('emailRequired'),
    password: validator('password'),
  });
};

export const registerFormValidation = () => {
  return Yup.object().shape({
    email: validator('emailRequired'),
    password: validator('password'),
    phoneNumber: validator('phone'),
    confirmPassword: validator('passwordConfirmation'),
  });
};
