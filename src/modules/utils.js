export const authTokenKey = 'loginAccessToken';

export const cityArray = ['Ahmedabad', 'Surat', 'Vadodara', 'Jamnagar'];

export const isValidArray = data =>
  data && Array.isArray(data) && data.length > 0;

export const isValidObject = data =>
  typeof data === 'object' && data ? Object.keys(data).length > 0 : false;

export const getAuthToken = () => localStorage.getItem(authTokenKey);
export const getLocalStorageData = value => localStorage.getItem(value);

export const setAuthToken = value => localStorage.setItem(authTokenKey, value);
export const setLocalStorageData = (key, value) =>
  localStorage.setItem(key, value);

export const removeItem = value => localStorage.removeItem(value);

export const getRandomString = (length = 15) => {
  const characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;

  let result = '';
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }

  return result;
};
