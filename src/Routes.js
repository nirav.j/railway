import React from 'react';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';

import AuthLayout from './AuthLayout';
import Login from './components/Login';
import Register from './components/Register';
import { getAuthToken } from './modules/utils';

const SkipIfAuthRoute = props => {
  const hasAuthToken = !!getAuthToken();

  if (hasAuthToken) {
    return <Redirect to="/dashboard" />;
  }

  return <Route {...props} />;
};

const AuthRoute = props => {
  const hasAuthToken = !!getAuthToken();

  if (hasAuthToken) {
    return <Route {...props} />;
  }

  return <Redirect to="/login" />;
};

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <SkipIfAuthRoute path="/login" component={Login} />
      <SkipIfAuthRoute path="/register" component={Register} />
      <AuthRoute path="/" component={AuthLayout} />
    </Switch>
  </BrowserRouter>
);

export default Routes;
