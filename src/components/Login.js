import { Formik } from 'formik';
import React from 'react';
import { Link, useHistory } from 'react-router-dom';

import { setLocalStorageData, setAuthToken } from '../modules/utils';
import { displayFormErrors, loginFormValidation } from '../modules/validation';
import FormField from './common/FormField';
import SpinnerButton from './common/SpinnerButton';

const initialValues = {
  email: '',
  password: '',
};

const Login = () => {
  const history = useHistory();

  const handleSubmit = values => {
    const role = values?.email === 'admin@gmail.com' ? 'admin' : 'user';
    setLocalStorageData('userRole', role);
    setAuthToken('userToken');

    history.push('/dashboard');
  };

  return (
    <div className="login-page">
      <div className="from-content">
        <h1>Log in</h1>
        <Formik
          initialValues={initialValues}
          validationSchema={loginFormValidation}
          onSubmit={handleSubmit}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleSubmit,
            submitCount,
          }) => {
            const showError = key =>
              displayFormErrors(key, errors, touched, submitCount);
            return (
              <form onSubmit={handleSubmit}>
                <div className="from">
                  <div className="from-list">
                    <FormField
                      handleChange={handleChange}
                      isRequired={true}
                      label="Email"
                      placeholder="Enter Email"
                      name="email"
                      type="email"
                      value={values.email}
                      id="registerEmail"
                      showError={showError}
                    />
                  </div>
                  <div className="from-list">
                    <FormField
                      handleChange={handleChange}
                      isRequired={true}
                      label="Password"
                      placeholder="Enter Password"
                      name="password"
                      type="password"
                      value={values.password}
                      id="registerPassword"
                      showError={showError}
                    />
                  </div>
                  <div className="submit-btn">
                    <SpinnerButton type="submit" label="Submit" />
                  </div>
                  <p className="tip signup-tip">
                    <span>Don't have an account?</span>
                    <Link to="/register">Sign up</Link>
                  </p>
                </div>
              </form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};

export default Login;
