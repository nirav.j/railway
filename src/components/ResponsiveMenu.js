import React, { useState } from 'react';
import { AiOutlineClose } from 'react-icons/ai';
import { FaAlignJustify } from 'react-icons/fa';
import { Link } from 'react-router-dom';
import dashboard from '../assets/images/dashboard.png';
import destination from '../assets/images/destination.png';
import { getLocalStorageData } from '../modules/utils';

const ResponsiveMenu = () => {
  const [isOpen, setIsOpen] = useState(false);

  const toggleMenu = () => setIsOpen(!isOpen);

  return (
    <div className="responsive-menu">
      <div className="responsive-menu-icon">
        <FaAlignJustify onClick={toggleMenu} />
      </div>
      {isOpen && (
        <div className="toggle-menu">
          <div className="close-toggle">
            <AiOutlineClose onClick={toggleMenu} />
          </div>
          <ul className="nav nav-pills nav-sidebar flex-column">
            <li className="nav-item">
              <Link className="nav-link" to="/dashboard">
                <img className="navbar-icon" src={dashboard} alt="" />
                Dashboard
              </Link>
            </li>
            {getLocalStorageData('userRole') === 'user' && (
              <li className="nav-item">
                <Link className="nav-link" to="/plan-journey">
                  <img className="navbar-icon" src={destination} alt="" />
                  Plan Journey
                </Link>
              </li>
            )}
          </ul>
        </div>
      )}
    </div>
  );
};

export default ResponsiveMenu;
