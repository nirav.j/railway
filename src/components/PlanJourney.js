import React, { useEffect, useRef, useState } from 'react';
import moment from 'moment';
import DatePicker from 'react-datepicker';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import { FormGroup } from 'reactstrap';
import {
  cityArray,
  getLocalStorageData,
  getRandomString,
  isValidArray,
  setLocalStorageData,
} from '../modules/utils';
import FormField from './common/FormField';
import SpinnerButton from './common/SpinnerButton';

const dataFormat = 'MMM DD, YYYY';

const PlanJourney = () => {
  const [source, setSource] = useState('');
  const [destination, setDestination] = useState('');
  const [sourceResult, setSourceResult] = useState([]);
  const [destinationResult, setDestinationResult] = useState([]);
  const [date, setDate] = useState(new Date());
  const [resultArray, setResultArray] = useState([]);
  const [isOpen, setIsOpen] = useState(false);

  const history = useHistory();
  const ref = useRef(null);

  useEffect(() => {
    let resultResult = [...cityArray];
    if (getLocalStorageData('stationArray')) {
      resultResult = [
        ...resultResult,
        ...JSON.parse(getLocalStorageData('stationArray')).map(
          d => d?.reservation,
        ),
      ];
    }

    setResultArray(resultResult);
  }, []);

  useEffect(() => {
    const result = resultArray.filter(d =>
      d.toLocaleLowerCase().includes(source),
    );

    setSourceResult(result);
  }, [source]);

  useEffect(() => {
    const result = resultArray.filter(d =>
      d.toLocaleLowerCase().includes(destination),
    );

    setDestinationResult(result);
  }, [destination]);

  const handleDestinationClick = data => {
    setDestination(data);
    openDatePicker();
  };

  const handleSourceClick = data => {
    ref?.current?.focus();
    setSource(data);
  };

  const handleCreateReservation = () => {
    const data = getLocalStorageData('userReservationArray');
    let userReservationArray = [
      {
        id: getRandomString(),
        source: source,
        destination: destination,
        date: moment(date).format(dataFormat),
      },
    ];
    if (data) {
      userReservationArray = [...userReservationArray, ...JSON.parse(data)];
    }
    setLocalStorageData(
      'userReservationArray',
      JSON.stringify(userReservationArray),
    );
    toast.success('Reservation successful!');
    history.push('/dashboard');
  };

  const openDatePicker = () => setIsOpen(!isOpen);

  return (
    <div className="body-content-right plan-journey">
      <div className="row">
        <div className="search-input">
          <div className="from-list">
            <FormField
              handleChange={e => setSource(e.target.value)}
              isRequired={false}
              placeholder="Enter Source"
              name="source"
              type="text"
              value={source}
              id="source"
            />
            {isValidArray(sourceResult) && source.length > 0 && (
              <div className="drp-listing">
                {sourceResult.map(d => (
                  <div key={d} onClick={() => handleSourceClick(d)}>
                    {d}
                  </div>
                ))}
              </div>
            )}
          </div>

          <div className="from-list">
            <FormGroup>
              <input
                type="text"
                name="destination"
                placeholder="Enter Destination"
                onChange={e => setDestination(e.target.value)}
                value={destination}
                ref={ref}
                className="form-control"
              />
            </FormGroup>
            {isValidArray(destinationResult) && destination.length > 0 && (
              <div className="drp-listing">
                {destinationResult.map(d => (
                  <div key={d} onClick={() => handleDestinationClick(d)}>
                    {d}
                  </div>
                ))}
              </div>
            )}
          </div>
        </div>
      </div>
      <div className="row journey-btn">
        <div className="col-6">
          <div className="date-picker full-width">
            <DatePicker
              selected={date}
              onChange={date => {
                setDate(date);
                openDatePicker();
              }}
              value={moment(date).format(dataFormat)}
              onClickOutside={openDatePicker}
              open={isOpen}
              onInputClick={openDatePicker}
            />
          </div>
        </div>
        <div className="col-6">
          <div className="submit-btn full-width">
            <SpinnerButton
              type="button"
              label="Create a Reservation"
              onClick={handleCreateReservation}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default PlanJourney;
