import React, { useEffect, useState } from 'react';
import DataTable from 'react-data-table-component';
import Pagination from 'react-js-pagination';

import Delete from '../assets/images/delete.png';

import { getLocalStorageData, setLocalStorageData } from '../modules/utils';
import FormField from './common/FormField';
import SpinnerButton from './common/SpinnerButton';
import AddStation from './modals/AddStation';

const Dashboard = () => {
  const [searchTerm, setSearchTerm] = useState('');
  const [isOpen, setIsOpen] = useState(false);
  const [mainDataArray, setMainDataArray] = useState([]);
  const [activePage, setActivePage] = useState(1);
  const [perPage, setPerPage] = useState(10);

  useEffect(() => {
    const { innerHeight } = window;

    const data = JSON.parse(getLocalStorageData('userReservationArray'));
    setMainDataArray(data);

    if (innerHeight < 550) {
      setPerPage(2);
    } else if (innerHeight < 750) {
      setPerPage(5);
    } else {
      setPerPage(10);
    }
  }, []);

  const getColumns = () => {
    return [
      {
        name: 'Source',
        selector: row => row.source,
      },
      {
        name: 'Destination',
        selector: row => row.destination,
      },
      {
        name: 'Date',
        selector: row => row.date,
      },
      {
        name: 'Action',
        selector: row => (
          <div className="delete-icon" onClick={() => handleDelete(row.id)}>
            <img src={Delete} alt="" />
          </div>
        ),
        center: true,
      },
    ];
  };

  const handleDelete = id => {
    const data = JSON.parse(getLocalStorageData('userReservationArray')).filter(
      d => d?.id !== id,
    );
    setLocalStorageData('userReservationArray', JSON.stringify(data));
    setMainDataArray(data);
  };

  const getFilterData = () => {
    if (searchTerm) {
      const value = searchTerm.toLowerCase();
      return mainDataArray.filter(
        ({ source, destination }) =>
          source.toLowerCase().search(value) !== -1 ||
          destination.toLowerCase().search(value) !== -1,
      );
    }
    return mainDataArray;
  };

  const filterData = getFilterData();

  const getTableData = () => {
    const indexOfLastData = activePage * perPage;
    const indexOfFirstData = indexOfLastData - perPage;
    const currentData = filterData.slice(indexOfFirstData, indexOfLastData);
    return currentData;
  };

  const tableData = getTableData();

  const toggle = () => setIsOpen(!isOpen);

  const handlePageChange = pageNumber => setActivePage(pageNumber);

  return (
    <div className="body-content-right">
      <div className="filter-btn-section d-flex justify-content-between">
        <div className="from-list">
          <FormField
            handleChange={e => setSearchTerm(e.target.value)}
            isRequired={false}
            placeholder="Search"
            name="searchTerm"
            type="text"
            value={searchTerm}
          />
        </div>
        {getLocalStorageData('userRole') === 'admin' && (
          <div className="submit-btn add-new-station">
            <SpinnerButton
              type="button"
              label="Add new station"
              onClick={toggle}
            />
          </div>
        )}
      </div>

      <div>
        <DataTable columns={getColumns()} data={tableData} />
      </div>

      <div className="pagination-section">
        <Pagination
          activePage={activePage}
          itemsCountPerPage={perPage}
          totalItemsCount={filterData.length}
          pageRangeDisplayed={5}
          onChange={handlePageChange}
        />
      </div>
      {isOpen && <AddStation isOpen={isOpen} toggle={toggle} />}
    </div>
  );
};

export default Dashboard;
