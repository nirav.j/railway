import { Formik } from 'formik';
import React from 'react';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import {
  displayFormErrors,
  registerFormValidation,
} from '../modules/validation';
import FormField from './common/FormField';
import SpinnerButton from './common/SpinnerButton';

const initialValues = {
  email: '',
  password: '',
  confirmPassword: '',
  phoneNumber: '',
};

const Register = () => {
  const history = useHistory();

  const handleSubmit = () => {
    toast.success('User registered successfully!');
    localStorage.setItem('loginAccessToken', 'userToken');
    history.push('/dashboard');
  };

  return (
    <div className="login-page register-page">
      <div className="from-content">
        <h1>Register Form</h1>
        <Formik
          initialValues={initialValues}
          validationSchema={registerFormValidation}
          onSubmit={handleSubmit}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleSubmit,
            submitCount,
          }) => {
            const showError = key =>
              displayFormErrors(key, errors, touched, submitCount);
            return (
              <form onSubmit={handleSubmit}>
                <div className="from">
                  <div className="from-list">
                    <FormField
                      handleChange={handleChange}
                      isRequired={true}
                      label="Email"
                      placeholder="Enter Email"
                      name="email"
                      type="email"
                      value={values.email}
                      id="registerEmail"
                      showError={showError}
                    />
                  </div>
                  <div className="from-list">
                    <FormField
                      handleChange={handleChange}
                      isRequired={true}
                      label="Phone Number"
                      placeholder="Enter Phone Number"
                      name="phoneNumber"
                      type="phoneNumber"
                      value={values.phoneNumber}
                      id="registerPhoneNumber"
                      showError={showError}
                    />
                  </div>
                  <div className="from-list">
                    <FormField
                      handleChange={handleChange}
                      isRequired={true}
                      label="Password"
                      placeholder="Enter Password"
                      name="password"
                      type="password"
                      value={values.password}
                      id="registerPassword"
                      showError={showError}
                    />
                  </div>
                  <div className="from-list">
                    <FormField
                      handleChange={handleChange}
                      isRequired={true}
                      label="Confirm Password"
                      placeholder="Enter Confirm Password"
                      name="confirmPassword"
                      type="confirmPassword"
                      value={values.confirmPassword}
                      id="registerConfirmPassword"
                      showError={showError}
                    />
                  </div>
                  <div className="submit-btn">
                    <SpinnerButton type="submit" label="Submit" />
                  </div>
                </div>
              </form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};

export default Register;
