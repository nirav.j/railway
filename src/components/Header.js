import React, { useState } from 'react';
import { FaUserCircle } from 'react-icons/fa';
import { useHistory, useLocation } from 'react-router-dom';
import {
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
} from 'reactstrap';
import { removeItem } from '../modules/utils';
import SpinnerButton from './common/SpinnerButton';

const Header = ({}) => {
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const location = useLocation();
  const history = useHistory();

  const { pathname } = location;

  const toggle = () => {
    setDropdownOpen(!dropdownOpen);
  };

  const getRouteName = () => {
    if (pathname.includes('/dashboard')) {
      return 'Dashboard';
    }
    if (pathname.includes('/plan-journey')) {
      return 'Plan Journey';
    }

    return null;
  };

  const handleLogout = () => {
    localStorage.removeItem('loginAccessToken');
    removeItem('loginAccessToken');
    removeItem('userRole');

    history.push('/login');
  };

  return (
    <div className="main-header navbar navbar-expand navbar-white navbar-light">
      <h3 className="header-title">Reservation App</h3>
      <h3 className="header-title">{getRouteName()}</h3>
      <Dropdown
        className="drp-btn-header"
        isOpen={dropdownOpen}
        toggle={toggle}
      >
        <DropdownToggle caret>
          <FaUserCircle />
          User@gmail.com
        </DropdownToggle>
        <DropdownMenu>
          <DropdownItem tag="div">
            <SpinnerButton
              type="button"
              label="Log Out"
              onClick={handleLogout}
            />
          </DropdownItem>
        </DropdownMenu>
      </Dropdown>
    </div>
  );
};

export default Header;
