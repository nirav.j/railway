import React from 'react';
import { Link } from 'react-router-dom';

import dashboard from '../assets/images/dashboard.png';
import destination from '../assets/images/destination.png';

import { getLocalStorageData } from '../modules/utils';

const Navbar = () => {
  return (
    <ul className="nav nav-pills nav-sidebar flex-column">
      <li className="nav-item">
        <Link className="nav-link" to="/dashboard">
          <img className="navbar-icon" src={dashboard} alt="" />
          Dashboard
        </Link>
      </li>
      {getLocalStorageData('userRole') === 'user' && (
        <li className="nav-item">
          <Link className="nav-link" to="/plan-journey">
            <img className="navbar-icon" src={destination} alt="" />
            Plan Journey
          </Link>
        </li>
      )}
    </ul>
  );
};

export default Navbar;
