import React from 'react';
import { FormGroup, Input, Label } from 'reactstrap';

const FormField = props => {
  const {
    handleChange,
    id,
    isRequired = false,
    label = '',
    name,
    placeholder,
    type = 'text',
    value,
    showError,
  } = props;

  return (
    <FormGroup>
      {label && (
        <Label for={id}>
          {label} {isRequired && <span className="text-danger">*</span>}
        </Label>
      )}
      <Input
        type={type}
        name={name}
        id={id}
        placeholder={placeholder}
        onChange={handleChange}
        value={value}
      />
      {showError?.(name)}
    </FormGroup>
  );
};

export default FormField;
