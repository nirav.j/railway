import React from 'react';
import LaddaButton, { SLIDE_UP, XL } from '@zumper/react-ladda';

const SpinnerButton = props => {
  const {
    type = 'button',
    label,
    spinnerSize = 30,
    spinnerColor = '#ddd',
    spinnerLines = 12,
    onClick,
  } = props;

  return (
    <LaddaButton
      size={XL}
      style={SLIDE_UP}
      spinnerSize={spinnerSize}
      spinnerColor={spinnerColor}
      spinnerLines={spinnerLines}
      type={type}
      onClick={onClick}
    >
      {label}
    </LaddaButton>
  );
};

export default SpinnerButton;
