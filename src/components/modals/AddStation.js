import React, { useState } from 'react';
import { toast } from 'react-toastify';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import {
  getLocalStorageData,
  getRandomString,
  setLocalStorageData,
} from '../../modules/utils';
import FormField from '../common/FormField';
import SpinnerButton from '../common/SpinnerButton';

const AddStation = ({ isOpen, toggle }) => {
  const [station, setStation] = useState('');

  const addStation = () => {
    const data = getLocalStorageData('stationArray');
    let stationArray = data ? JSON.parse(data) : [];

    stationArray = [
      ...stationArray,
      { id: getRandomString(), reservation: station },
    ];

    setLocalStorageData('stationArray', JSON.stringify(stationArray));

    toast.success('Station added!');
    toggle();
  };

  return (
    <Modal isOpen={isOpen} toggle={toggle}>
      <ModalHeader toggle={toggle}>Add station</ModalHeader>
      <ModalBody>
        <div className="from-list">
          <FormField
            handleChange={e => setStation(e.target.value)}
            placeholder="Enter Station Name"
            name="station"
            type="text"
            value={station}
          />
        </div>
      </ModalBody>
      <ModalFooter>
        <div className="submit-btn add-new-station">
          <SpinnerButton
            className="add-btn"
            type="button"
            label="Add new station"
            onClick={addStation}
          />
        </div>
        <Button color="cancel-btn secondary" onClick={toggle}>
          Cancel
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default AddStation;
